## Gmail revoke access
### 1.Open [https://myaccount.google.com/](https://myaccount.google.com/), Login to a Google account that needs to be revoked
### 2.Select Safely
![avatar](./revoke_gmail_picture/gmail_choose_safely_en.png)
### 3.Select Google apps with account access
![avatar](./revoke_gmail_picture/gmail_en_choose_app.png)
### 4.Select the application that needs to revoke the authorization and click revoke access
![avatar](./revoke_gmail_picture/gmail_en_revoke.png)