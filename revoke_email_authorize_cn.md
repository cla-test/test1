## Gmail撤销授权
### 1.打开[https://myaccount.google.com/](https://myaccount.google.com/) ，登陆需要撤销授权的Google账号
### 2.选择安全性
![avatar](./revoke_gmail_picture/gmail_cn_choose_security_menu.png)
### 3.选择具有账号权限访问的第三方应用
![avatar](./revoke_gmail_picture/gmail_cn_choose_app_menu.png)
### 4.选择需要撤销授权的应用，点击撤销访问权限
![avatar](./revoke_gmail_picture/gmail_cn_revoke.png)